package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestCliente {
	
	private static GestorContabilidad gestor;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("Manolo", "12345678T", LocalDate.parse("2000-12-12"));
		gestor.altaCliente(cliente);
	}

	@Test
	//Comprobamos si, tras buscar un cliente, su nombre coincide con el que deber�a ser.
	void testBuscarCliente() {
		Cliente actual = gestor.buscarCliente("12345678T");
		String esperado = "Manolo";
		assertEquals(esperado, actual.getNombre());
	}

	@Test
	//Comprobamos si, tras dar de alta al cliente y buscarlo, lo encuentra ya que NO devuelve null.
	void testAltaCliente() {
		gestor.altaCliente(new Cliente("Pepito", "98765432Q", LocalDate.parse("2013-12-03")));
		Cliente actual = gestor.buscarCliente("98765432Q");
		assertNotNull(actual);
	}

	@Test
	void testClienteMasAntiguo() {
		fail("Not yet implemented");
	}

	@Test
	//Comprobamos si, tras eliminar el cliente, devuelve null al buscarlo, ya que no deber�a de existir.
	void testEliminarCliente() {
		gestor.eliminarCliente("12345678T");
		Cliente actual = gestor.buscarCliente("12345678T");
		assertNull(actual);
	}

}
