package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Factura;
import clases.GestorContabilidad;

class TestFactura {
	
	private static GestorContabilidad gestor;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
		Factura factura = new Factura("1", LocalDate.parse("2000-12-11"), "teclado", 12.30, 2, gestor.buscarCliente("12345678T"));
		Factura otraFactura = new Factura("3", LocalDate.parse("2000-12-11"), "teclado", 10, 1, gestor.buscarCliente("12345678T"));
		gestor.crearFactura(factura);
		gestor.crearFactura(otraFactura);
	}


	@Test
	//Comprobamos si busca la factura que hemos pasado al principio del test gracias al precio total, que multiplica 12.30 por 2 (24.60).
	// Como hemos comprobado en otro Test que este m�todo funciona, lo podemos utilizar para comprobar este otro.
	void testBuscarFactura() {
		Factura actual = gestor.buscarFactura("1");
		double esperado = 24.60;
		assertEquals(esperado, actual.precioTotal());
	}

	@Test
	//Comprobamos que el if que checkea el codigo (para no repetirse) funciona correctamente
	void testCrearFactura() {
		int cont=0;
		for (Factura factura : gestor.getListaFacturas()){
			if(factura.getCodigoFactura().equals("1")) {
				cont++;
			}
		}
		int esperado = 1;
		int actual = cont;
		assertEquals(esperado, actual);
	}

	@Test
	// Creamos una nueva factura, que es obviamente m�s cara que la primera que hab�amos pasado, y comprobamos si est� cogi�ndola correctamente o no
	void testFacturaMasCara() {
		
		Factura factura = new Factura("2", LocalDate.parse("2000-12-11"), "teclado", 34.30, 5, gestor.buscarCliente("12345678T"));
		gestor.crearFactura(factura);
		Factura masCara = gestor.facturaMasCara();
		Factura esperada = masCara;
		Factura actual = gestor.buscarFactura("2");
		assertEquals(esperada, actual);
	}

	@Test
	// Comprobamos si la facturaci�n anual corresponde con la que deber�a ser para las facturas del a�o 2000.
	void testCalcularFacturacionAnual() {
		
		double esperado = 34.60;
		double actual = gestor.calcularFacturacionAnual(2000);
		
		assertEquals(esperado, actual);
	}

	@Test
	//Mismo planteamiento que con EliminarCliente en el otro Test, pero con Factura.
	void testEliminarFactura() {
		gestor.eliminarFactura("1");
		Factura actual = gestor.buscarFactura("1");
		assertNotNull(actual);
	}

}
