package tests;




import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import clases.Cliente;
import clases.Factura;

class TestPrecioTotal {

	@Test
	//Vamos a comprobar si funciona el �nico m�todo de la clase facturas
	void test() {
		Cliente cliente = new Cliente ("Manolo", "12345678T", LocalDate.parse("2000-12-12"));
		Factura factura = new Factura("1", LocalDate.parse("2000-12-11"), "teclado", 12.30, 2, cliente);
		double esperado = 24.60;
		double actual = factura.precioTotal();
		assertEquals(esperado, actual, 0.0);
	}

}
