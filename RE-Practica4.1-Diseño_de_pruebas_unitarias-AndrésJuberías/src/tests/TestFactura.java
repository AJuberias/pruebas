package tests;



import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

class TestFactura {
	
	private static GestorContabilidad gestor;

	@BeforeClass
	static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
		Factura factura = new Factura("1", LocalDate.parse("2000-12-11"), "teclado", 12.30, 2, gestor.buscarCliente("12345678T"));
		Factura otraFactura = new Factura("3", LocalDate.parse("2000-12-11"), "teclado", 10, 1, gestor.buscarCliente("12345678T"));
		gestor.crearFactura(factura);
		gestor.crearFactura(otraFactura);
	}


	@Test
	
	void testBuscarFactura() {
		
		Factura unaFactura = new Factura("4", LocalDate.parse("2000-10-10"),"teclado",3,1,gestor.buscarCliente("12345678T"));
		Factura actual = gestor.buscarFactura("4");
		Factura esperado = unaFactura;
		assertSame(actual,esperado);
	//Edit: Ahora busca Objetos en vez de comparar precios
	}

	@Test
	//Comprobamos que el if que checkea el codigo (para no repetirse) funciona correctamente
	void testCrearFactura() {
		int cont=0;
		for (Factura factura : gestor.getListaFacturas()){
			if(factura.getCodigoFactura().equals("1")) {
				cont++;
			}
		}
		int esperado = 1;
		int actual = cont;
		assertEquals(esperado, actual);
	}

	@Test
	// Creamos una nueva factura, que es obviamente m�s cara que la primera que hab�amos pasado, y comprobamos si est� cogi�ndola correctamente o no
	void testFacturaMasCara() {
		
		Factura factura = new Factura("2", LocalDate.parse("2000-12-11"), "teclado", 34.30, 5, gestor.buscarCliente("12345678T"));
		gestor.crearFactura(factura);
		Factura masCara = gestor.facturaMasCara();
		Factura esperada = masCara;
		Factura actual = gestor.buscarFactura("2");
		assertSame(esperada, actual);
	//Edit: assertSame en vez de assertEquals al tratarse de Objetos.
	}

	@Test
	// Comprobamos si la facturaci�n anual corresponde con la que deber�a ser para las facturas del a�o 2000.
	void testCalcularFacturacionAnual() {
		
		double esperado = 34.60;
		double actual = gestor.calcularFacturacionAnual(2000);
		
		assertEquals(esperado, actual, 0.0);
	}

	@Test
	//Mismo planteamiento que con EliminarCliente en el otro Test, pero con Factura.
	void testEliminarFactura() {
		gestor.eliminarFactura("1");
		Factura actual = gestor.buscarFactura("1");
		assertNull(actual);
	}

}
