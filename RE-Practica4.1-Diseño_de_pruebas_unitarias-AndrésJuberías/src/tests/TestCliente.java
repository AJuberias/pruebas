package tests;



import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestCliente {
	
	private static GestorContabilidad gestor;

	@BeforeClass
	static void setUpBeforeClass() throws Exception {
		
		gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("Manolo", "12345678T", LocalDate.parse("2000-12-12"));
		Cliente otroCliente = new Cliente("Alvaro","432123464Q", LocalDate.now());
		gestor.altaCliente(cliente);
		gestor.altaCliente(otroCliente);
	}

	@Test
	//Comprobamos si, tras buscar un cliente, su nombre coincide con el que deber�a ser.
	void testBuscarCliente() {
		
		Cliente actual = gestor.buscarCliente("12345678T");
		String esperado = "Manolo";
		assertEquals(esperado, actual.getNombre());
	}

	@Test
	//Comprobamos si, tras dar de alta al cliente y buscarlo es el mismo que el esperado.
	void testAltaCliente() {
		
		Cliente nuevoCliente = new Cliente("Pepito", "98765432Q", LocalDate.parse("2013-12-03"));
		gestor.altaCliente(nuevoCliente);
		Cliente actual = gestor.buscarCliente("98765432Q");
		Cliente esperado = nuevoCliente;
		assertSame(esperado, actual);
	}

	@Test
	// Comprobamos si busca el cliente m�s antiguo, que tras escribirlos manualmente tendr�a que ser el
	// que pasamos como cliente esperado.
	void testClienteMasAntiguo() {
		
		Cliente actual = gestor.clienteMasAntiguo();
		Cliente esperado = gestor.buscarCliente("12345678T");
		assertSame(esperado, actual);
	//Edit: assertSame en vez de equals al tratarse de Objetos.
	}

	@Test
	//Comprobamos si, tras eliminar el cliente, devuelve null al buscarlo, ya que no deber�a de existir.
	void testEliminarCliente() {
		
		gestor.eliminarCliente("12345678T");
		Cliente actual = gestor.buscarCliente("12345678T");
		assertNull(actual);
	//Edit: assertNull en vez de assertNotNull, ya que al eliminarlo no deber�a existir
	}
	
	
	//Al eliminar un cliente no deber�a haber facturas a su nombre, as� que comprobamos si al eliminarlo
	void testEliminarClienteFacturasPendientes(){
		
		Cliente cliente = gestor.buscarCliente("12345678T");
		
		Factura factura = new Factura("2", LocalDate.parse("2000-12-11"), "teclado", 34.30, 5, cliente);
		gestor.crearFactura(factura);
		
		gestor.eliminarCliente("12345678T");
		
		int cont=0;
		for (Factura unaFactura : gestor.getListaFacturas()){
			if (unaFactura.getCliente().equals(cliente.getNombre())){
				cont++;
			}
		}
		int esperado = 0;
		int actual = cont;
		assertEquals(esperado, actual);
		//Edit: m�todo entero a�adido
	}

}
