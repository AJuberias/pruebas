package clases;

import java.time.LocalDate;

public class Cliente {

		private String nombre;
		private String DNI;
		private LocalDate fechaAlta;
		
		public Cliente(String nombre, String DNI, LocalDate fechaAlta) {
			this.nombre=nombre;
			this.DNI=DNI;
			this.fechaAlta=fechaAlta;
		}
		
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getDNI() {
			return DNI;
		}
		public void setDNI(String dNI) {
			DNI = dNI;
		}
		public LocalDate getFechaAlta() {
			return fechaAlta;
		}
		public void setFechaAlta(LocalDate fechaAlta) {
			this.fechaAlta = fechaAlta;
		}
		
		@Override
		public String toString() {
			String cadena=nombre+" "+DNI+" "+fechaAlta;
			return cadena;
		}
}
