package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	public Cliente buscarCliente(String DNI) {
		for (Cliente cliente : listaClientes) {
			if(cliente.getDNI().equals(DNI)) {
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura (String codigo) {
		for (Factura factura : listaFacturas) {
			if(factura.getCodigoFactura().equals(codigo)) {
				return factura;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente nuevoCliente) {
		int cont=0;
		for(Cliente cliente : listaClientes) {
			if(nuevoCliente.getDNI().equals(cliente.getDNI())) {
				cont++;
			}
		}
		
		if(cont==0) {
			listaClientes.add(nuevoCliente);
		}
		listaClientes.add(nuevoCliente);
	}
	
	public void crearFactura(Factura nuevaFactura) {
		int cont=0;
		for(Factura factura : listaFacturas) {
			if(nuevaFactura.getCodigoFactura().equals(factura.getCodigoFactura())) {
				cont++;
			}
		}
		if(cont==0) {
			listaFacturas.add(nuevaFactura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		
		String DNIauxiliar="";
		LocalDate fechaAuxiliar=LocalDate.now();
		for (Cliente cliente : listaClientes) {
			if(cliente.getFechaAlta().isAfter(fechaAuxiliar)) {
				DNIauxiliar=cliente.getDNI();
			}
		}
		return buscarCliente(DNIauxiliar);
	}
	
	public Factura facturaMasCara() {
		
		String codigo="";
		double precioAuxiliar=0;
		for (Factura factura : listaFacturas) {
			if(factura.precioTotal()>precioAuxiliar) {
				codigo=factura.getCodigoFactura();
			}
		}
		
		return buscarFactura(codigo);
	}
	
	public float calcularFacturacionAnual(int anno) {
		
		float facturaTotal=0;
		for (Factura factura : listaFacturas) {
			if(factura.getFecha().getYear()==anno) {
				facturaTotal=(float)(facturaTotal+factura.precioTotal());
			}
		}
		return facturaTotal;
	}
	
	
	public void asignarClienteAFactura(String DNI, String codigoFactura) {
		buscarFactura(codigoFactura).setCliente(buscarCliente(DNI));
	}
	
	public int cantidadFacturasPorClientes(String DNI) {
		
		int cont=0;
		for (Factura factura : listaFacturas) {
			if(factura.getCliente().equals(buscarCliente(DNI))) {
				cont++;
			}
		}
		return cont;
	}
	
	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}
	
	public void eliminarCliente(String dni) {
		listaClientes.remove(buscarCliente(dni));
	}
}
